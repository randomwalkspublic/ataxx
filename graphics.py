import pygame as pg
import numpy as np
from os import path
from core import (Game, Move, list_plays_from_cell, apply, skip_turn,
                  search_greedy_move, minimax, std_eval, alt_eval, scoring, save_state)

## Data Structure extension

class GGame (Game):
  '''
  extra variables for graphical state:
  cell_size :: to locate in the screen the upper left corner of the cell in row, col
  cells2update :: to only update in the screen what is stricly necessary
                  initalized to None to force update all
  selected :: to control wether 1st or 2nd click
  destinations :: to paint destinations highlight
  search :: {'eval_fun': eval_function, 'fun': search_greedy_move or minimax, 'depth': depth, 'cuts': True or False }
  '''

  def __init__(self, size_board = 7, screen_size = 800):
    Game.__init__(self, size_board)
    self.cell_size = screen_size // size_board
    self.cells2update = None
    self.selected = []
    self.destinations = []

    # search dict will be so that pc_player knows which search function to use
    self.search = dict()


## Paths and load functions

game_dir = path.split(path.abspath(__file__))[0]
assets_dir = path.join(game_dir, "1.assets")

def load_image(filename: str, cell_size, colorkey=None):
  fullname = path.join(assets_dir, filename)
  image = pg.image.load(fullname)
  image = image.convert()
  image = pg.transform.scale(image, (cell_size, cell_size))

  if colorkey is not None:
    center = image.get_rect().center
    colorkey = image.get_at( center )
    image.set_colorkey(colorkey, pg.RLEACCEL)

  return image


def board_load(ficheiro: str, screen_size):

  try:
    fd = open(ficheiro, 'r', encoding='utf-8')
  except IOError:
    print('File not found')
    # a retornar 0 mas vai contra o declared type de return na
    # function signature
    return 0

  # ler a primeira linha
  size_board, n_move, turn  = map( int, fd.readline().split() )

  # cria jogo com tamanho de board correto
  game = GGame(size_board, screen_size)
  game.n_move = n_move
  game.turn = turn

  for k in range(size_board):
    line_values = list( map( int, fd.readline().split() ))
    game.board[k] = line_values

  # update score
  game.score_1, game.score_2 = scoring(game.board)

  return game

  
## Graphical board drawing

def update_screen(screen, game, assets: dict) -> None:
  '''
  board is represented by tiles that have the pieces built in
  image updates are only where changes to board state have happened + highlighted cells
  '''

  # variable extraction for terseness
  size_board = len(game.board)
  board = game.board
  cell_size = game.cell_size
  cells2update = game.cells2update
  
  # first time to draw must be the whole board
  # so create a list of AxA where A = {0, ... , size_board - 1}
  if cells2update == None:
    cells2update = game.cells2update = []
    for lin in range(size_board):
      for col in range(size_board):

        cells2update.append( [lin,col] )

  # update all mutated pieces (from move and highlight)
  for lin, col in cells2update:
    
    if board[lin][col] == 0:
      screen.blit( assets["empty"], (col*cell_size, lin*cell_size) )

    elif board[lin][col] == 1:
      screen.blit( assets["blue"], (col*cell_size, lin*cell_size) )
      
    elif board[lin][col] == 2:
      screen.blit( assets["red"], (col*cell_size, lin*cell_size) )

    elif board[lin][col] == 8:
      screen.blit( assets["wall"], (col*cell_size, lin*cell_size) )

  # board update is done, so cells2update is cleared
  game.cells2update = cells2update = []

  if game.selected  != []:
    lin, col = game.selected
    screen.blit( assets["selected"], (col*cell_size, lin*cell_size) )

  if game.destinations != []:
    for [lin, col] in game.destinations:
      screen.blit( assets["destination"], (col*cell_size, lin*cell_size) )

  # switch displayed buffer
  pg.display.flip()
    
  return


## Select cell from mouse click and play

def select_cell_and_play(game, event) -> None:
  '''
  mutates game.selected, game.cells2update
  '''
  
  cell_x = int(event.pos[0] // game.cell_size)
  cell_y = int(event.pos[1] // game.cell_size)

  ## Handling 1st click
  # can happen when nothing is selected
  # or another player piece is clicked on
  if game.board[cell_y][cell_x] == game.turn:
    # in case a piece was selected it must be marked to be cleared of highlighting
    # along with the destinations highlight
    if game.selected != []:
      game.cells2update.append( game.selected )
      game.cells2update.extend( game.destinations)

    game.selected = [ cell_y, cell_x ]
    game.destinations = list_plays_from_cell(game, cell_y, cell_x)

  ## Handling 2nd click
  # when clicking on an empty cell, having already a start position selected
  elif game.board[cell_y][cell_x] != game.turn and game.selected != []:
      lin_i, col_i = game.selected
      move = Move(lin_i, col_i, cell_y, cell_x)
      
      # the move might not be valid if distance is too big, in that case [] is returned and nothing
      # is added to cells2update
      game.cells2update.extend( apply(move, game) ) # move and marking board cells for update
      # whether move was valid or not a deselection will occur
      game.cells2update.extend( game.destinations )
      game.cells2update.append( game.selected )

      game.selected = []       # clearing selected cell
      game.destinations = []   # clearing destinations

  else:
    # only enters here if click is not on a owned piece while having nothing selected
    pass

  return


from math import inf
import time

def pc_player(game) -> bool:

  # DEBUG
  #print(game.search)
  
  eval_fun = game.search['eval_fun']
  search = game.search['fun']

  
  # DEBUG
  #name = 'start greedy search time: '
  #st = time.process_time_ns()
  if game.search['depth'] != None:
    # then minimax is selected
    depth = game.search['depth']
    if game.search['cuts'] == True:
      alpha = -inf
      beta  = +inf
    else:
      alpha = beta = None

    _, move, _ = search(game, eval_fun, depth, alpha, beta, 0)  

  else:
    move = search(game)
  #et = time.process_time_ns()
  #print(name, (et-st)//1000, 'µs')

  from core import list_moves_player
  if move == 0:
    skip_turn(game)
    return False # meaning it isn't a human player

  # DEBUG
  #name = 'do actualy move time: '
  #st = time.process_time_ns()
  game.cells2update.extend( apply(move,game) ) # move and marking board cells for update 
  #et = time.process_time_ns()
  #print(name, (et-st)//1000, 'µs')
  
  return False # meaning it isn't a human player


def human_player(game):
  return True  # meaning it's a human player


## CLI select game type, pc_player_type and board size

def game_type_def():
  '''
  Decidir:
  Humano vs Humano
  Humano vs PC
  PC     vs Humano
  PC     vs PC
  '''

  FPS = 60
  
  while True:
    message = '''\n\
1 :: Human vs Human
2 :: Human vs PC
3 :: PC vs Human
4 :: PC vs PC'''
    print(message)
    print("Choose game type (default = 2): ", end = '')
    choice = input()

    if choice == '': choice = '2'
    

    if choice == '1':
      return (human_player, human_player, FPS)
    if choice == '2':
      return (human_player, pc_player, FPS)
    if choice == '3':
      return (pc_player, human_player, FPS)
    if choice == '4':
      return (pc_player,pc_player, 20)


def pc_player_type(game):
  '''
  Decidir:
  Greedy ou Minimax
  Com ou sem cortes alpha beta
  '''

  search = game.search
  
  message = '''\n\
1 n :: Minimax with Alpha/Beta Cuts (depth = n)
2 n :: Pure Minimax (depth = n)
3   :: Greedy

A :: Standard Evaluation Function: piece count
B :: Alternative Evaluation: protected pieces count double!    
'''
  print(message)
  print("Choose PC player type (default = 1 3 A): ", end = '')
  stdin = input()

  # setting default
  if stdin == '': stdin = '1 3 A'

  choices = stdin.split()

  if choices[0] == '1':
    search['fun'] = minimax
    search['cuts'] = True
    search['depth'] = int( choices[1] )
  elif choices[0] == '2':
    search['fun'] = minimax
    search['cuts'] = False
    search['depth'] = int( choices[1] )
  elif choices[0] == '3':
    search['fun'] = search_greedy_move
    search['depth'] = None
    search['eval_fun'] = std_eval
    return # greedy é sempre com std_eval

  if choices[2] == 'A':
    search['eval_fun'] = std_eval
  elif choices[2] == 'B':
    search['eval_fun'] = alt_eval
    

def board_size_def():
  '''
  Choose board size
  '''
  print("Size of board (default = 7): ", end = '')
  size = input()

  if size == '':
    return 7

  return int(size)


def choose_load_or_new():
  clear()
  print("Load or start a new board?")
  print("l: load, n: new (default = new): ", end = '')
  choice = input()

  while True:
    if choice == '' or choice == 'n':
      return 0
    if choice == 'l':
      return 1


def graphical_board_load(screen_size):
  while True:
    print("Insert file name (default = 'save1': ", end = '')
    name = input()
    if name == '': name = 'save1'
  
    filename = f'0.saves/{name}.txt'
    
    game = board_load(filename, screen_size)
    if game != 0:
      return game
  

def graphical_board_save(game):
  print("Insert file name (default = 'save1': ", end = '')
  name = input()
  if name == '': name = 'save1'
  filename = f'0.saves/{name}.txt'

  print(save_state(filename, game))
  print('File saved!')
  print('Leave or continue playing.\n')
  return


from os import system, name

def clear():
    # for windows
    if name == 'nt': system('cls')
    # for mac and linux(here, os.name is 'posix')
    else: system('clear')

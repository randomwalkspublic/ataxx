from typing import Type

### Data Structures
import numpy as np

class Game:
  '''
  Create full game state -> board, n_move, turn, score
  '''
  def __init__(self, size_board = 7, uninitialized = False):
    if uninitialized: return
    # default board 7x7 with no walls

    board = np.zeros((size_board,size_board), dtype=int)

    # place blue (0,0), (fim,fim)
    # place red  (0, fim), (fim,0)

    board[0][0]   = 1
    board[-1][-1] = 1
    board[0][-1]  = 2
    board[-1][0]  = 2

    self.board = board
    self.n_move = 0
    self.turn = 1
    # mesmo usando outras evals, deixa-se isto porque
    # contabiliza peças de cada jogador
    # usado em plays_available
    self.score_1 = 2
    self.score_2 = 2

  def __repr__(self):
    board = str(self.board)
    n_move = 'n_move: ' + str(self.n_move)
    turn   = ' turn: ' + str(self.turn)
    scores = ' score: ' + str(self.score_1) + ', ' + str(self.score_2)
    return board + '\n' + n_move + turn + scores


class Move:
  '''
  vetor com início e fim
  coordenadas negativas é a contar do fim das linhas ou colunas
  '''
  def __init__(self, lin_i: int, col_i: int, lin_f: int, col_f: int):
    self.start = [lin_i, col_i]
    self.end   = [lin_f, col_f]

  def __repr__(self):
    return f"{self.start} ⇒ {self.end}"

  
## Neighbourhood function

def neighbourhood_action(**kwargs) -> int:
  '''
  retorna 0: não iterou a vizinhança toda
  retorna 1:     iterou a vizinhança toda

  calling signature mandatory:
  game, lin_origin, col_origin, displacement_size, action

  action guaranteed to receive:
  game, lin_origin, col_origin, adj_lin, adj_col

  action(game: Type[Game], adj_lin: int, adj_col: int) -> int
  [adj_lin, adj_col] é o ponto adjacente em que action vai agir
  action return 0 -> exit loop
  action return 1 -> keep at it
  action não pode retornar None, ex.: haver uma ausência de return
  porque not None -> True ou seja era idêntico a ter retornado 0 e
  sai do loop
  '''
  displacement_size = kwargs.pop('displacement_size')
  action = kwargs.pop('action')
  
  lin_origin = kwargs['lin_origin']; col_origin = kwargs['col_origin']
  size_board = len(kwargs['game'].board)


  for dlin in displacement_size:
    for dcol in displacement_size:

      adj_lin = lin_origin + dlin
      adj_col = col_origin + dcol

      # origem não interessa na pesquisa
      if dlin == 0 and dcol == 0: continue
      # se indice for negativo não são candidatos adjacentes
      if adj_lin < 0 or adj_col < 0: continue
      # fora do tabuleiro
      if adj_lin >= size_board or adj_col >= size_board: continue


      kwargs['adj_lin'] = adj_lin
      kwargs['adj_col'] = adj_col
      # the return value of action dictates whether we continue on the loop
      # or return from it
      stay = action(**kwargs)
      if not stay: return 0

  return 1


def conquer_adjacent(**kwargs) -> int:

  game = kwargs['game']
  lin = kwargs['lin_origin']; col = kwargs['col_origin'];
  adj_lin = kwargs['adj_lin']; adj_col = kwargs['adj_col'];
  mutation = kwargs['mutation']
  
  # se destino + v for do oponente então é conquistada
  # código oponente é 3 - game.turn
  # turn = 1 -> 3 - 1 = 2
  # turn = 2 -> 3 - 2 = 1
  
  if game.board[ adj_lin ][ adj_col ]  != (3 - game.turn):
    return 1 # continua a correr a vizinhança
    
  game.board[ adj_lin ][ adj_col ] = game.turn
  mutation.append([ adj_lin, adj_col ])
  
  score = f'score_{game.turn}'
  setattr(game, score, getattr(game, score) + 1 )
  score = f'score_{3-game.turn}'
  setattr(game, score, getattr(game, score) - 1 )

  # retorna 1 simboliza para continuar na iteração da vizinhança
  return 1


def cell_has_player_piece(**kwargs):
    # retorna zero para sair da iteração da vizinhança pois encontrou
    # origem possível

    game = kwargs['game']
    adj_lin = kwargs['adj_lin']; adj_col = kwargs['adj_col'];
    players = kwargs['players']

    if game.board[ adj_lin ][ adj_col ] in players: return 0
    else:                                           return 1


def empty_cell(**kwargs) -> int:
    '''se célula vazia, acrescenta à lista de plays
    retorna sempre 1 pois a vizinhança é para iterar toda'''

    game = kwargs['game']
    adj_lin = kwargs['adj_lin']; adj_col = kwargs['adj_col'];
    plays = kwargs['plays']


    if game.board[adj_lin][adj_col] == 0: plays.append( [adj_lin, adj_col] )
    return 1


## State initialization helper functions

def scoring(board: list) -> list:
  '''
  calculate the score of each player when loading
  '''
  player1 = 0
  player2 = 0

  size_board = len(board)

  for lin in range(size_board):
    for col in range(size_board):

      if board[lin][col] == 1:
        player1 += 1
      elif board[lin][col] == 2:
        player2 += 1

  return [player1, player2]


def load_state(ficheiro: str) -> Type[Game]:

  try:
    fd = open(ficheiro, 'r', encoding='utf-8')
  except IOError:
    print('File not found')
    # a retornar 0 mas vai contra o declared type de return na
    # function signature
    return 0

  # ler a primeira linha
  size_board, n_move, turn  = map( int, fd.readline().split() )

  # cria jogo com tamanho de board correto
  game = Game(size_board)
  game.n_move = n_move
  game.turn = turn

  for k in range(size_board):
    line_values = list( map( int, fd.readline().split() ))
    game.board[k] = line_values

  # update score
  game.score_1, game.score_2 = scoring(game.board)

  return game


def save_state(ficheiro: str, game: Type[Game]) -> int:

  try:
    fd = open(ficheiro, 'w', encoding='utf-8')
  except IOError:
    print('File not found')
    return 0

  N = len(game.board)
  output = '%d %d %d\n' % (N, game.n_move, game.turn)
  fd.write(output)

  for k in range(N):
    line = ' '.join( map(str, game.board[k]))
    fd.write(line)
    fd.write('\n')

  return 1


## Validate and Apply moves

def distance(start_lin, start_col, end_lin, end_col) -> tuple[int]:
  '''
  retorna (distancia ortogonal x, distancia ortogonal y)
  '''
  
  return abs(start_lin - end_lin), \
         abs(start_col - end_col)


def type_move_from_distance(start_lin, start_col, end_lin, end_col):
  '''
  tipo salto -> retorna 2
  tipo clone -> retorna 1
  too far    -> retorna 0
  '''
  
  dist_lin, dist_col = distance(start_lin, start_col, end_lin, end_col)
  if dist_lin >= dist_col: maior = dist_lin
  else:                    maior = dist_col

  if   maior == 1: return 1
  elif maior == 2: return 2
  else:            return 0


def valid(move: Type[Move], game: Type[Game]) -> int:
  '''
  retorna 0 :: movimento inválido
  retorna 1 :: válido e expansão peça
  retorna 2 :: válido e salto peça

  mutação de move caso esteja fora do tabuleiro
  '''
  size_board = len(game.board)

  # posições start e end têm de estar no tabuleiro

  # de forma a aceitar coordenadas negativas a dizer
  # que começa no fim de uma linha/coluna
  # faço congruências modulo size_board

  for k in range(2):
    move.start[k] %= size_board
    move.end[k]   %= size_board

  # posição inicial tem de ter uma peça do jogador que tem a vez
  if game.board[ move.start[0] ][ move.start[1] ] != game.turn: return 0

  # posição final não pode ter uma peça
  if game.board[ move.end[0] ][ move.end[1] ] != 0: return 0

  # a maior distancia ortogonal <= 2
  # se maior 1 -> expansão peça
  # se maior 2 -> salto peça

  return type_move_from_distance(move.start[0], move.start[1], move.end[0], move.end[1])


def apply(move: Type[Move], game: Type[Game]) -> list:
  '''
  Aplica o move no tabuleiro
  retorna lista de [lin, col] onde hove alterações :: se o move era válido -> houve mutação
  esta lista de retorno é informação para atualizar parte gráfica sem ter de fazer update a
  todo o tabuleiro
  retorna [] :: se o move era inválido -> não houve mutação
  '''
  
  type_move = valid(move, game)

  # se move for inválido não faz nada e devolve 0
  if type_move == 0: return []

  mutation = []
  
  # seja expansão ou salto o destino fica com peça do jogador
  game.board[ move.end[0] ][ move.end[1] ] = game.turn
  mutation.append([ move.end[0], move.end[1] ])

  if type_move == 1:
    # se foi expansão então o score tem de incrementar
    score = f'score_{game.turn}'
    setattr(game, score, getattr(game, score) + 1 )
    
  elif type_move == 2:
    # se foi salto a origem fica vazia
    game.board[ move.start[0] ][ move.start[1] ] = 0
    mutation.append( [move.start[0], move.start[1]] )

  # conquistar adjacentes da posição de destino move.end = [lin_f, col_f]
  neighbourhood_action(game=game, lin_origin=move.end[0], col_origin=move.end[1], \
                       displacement_size=[-1,0,1], action=conquer_adjacent, mutation=mutation)

  # update n_moves and turn
  game.turn = 3 - game.turn
  game.n_move += 1
  
  return mutation


def skip_turn(game: Type[Game]) -> int:
  '''
  retorna 0 :: se passar a vez não é permitido
  retorna 1 e troca a vez (não muda o número da jogada)
  :: se passar a vez for permitido 
  '''

  # passar a vez apenas é possível se o jogador atual não tiver moves
  # possíveis, isto no entanto não garante que esse jogador perde daí
  # skip_turn ter de existir

  if plays_available(game, [game.turn]): return 0

  # não há moves possíveis -> passa a vez
  print("Skipped turn")
  game.turn = 3 - game.turn
  return 1


## General Helper Functions

def std_eval(game):
  '''
  Vantagem para quem tem mais peças
  Player 1 -> maximing
  Player 2 -> minimizing
  '''
  
  return game.score_1 - game.score_2


def not_protected(**kwargs) -> int:
    '''se célula vazia, acrescenta à lista de plays
    retorna sempre 1 pois a vizinhança é para iterar toda'''

    game = kwargs['game']
    adj_lin = kwargs['adj_lin']; adj_col = kwargs['adj_col'];

    if game.board[adj_lin][adj_col] == 0: return 0 # encontrou peça vazia sai do loop
    return 1


def alt_eval(game):
  '''
  se uma peça do jogador não tem vazio à volta está protegida
  por isso vale o dobro
  '''
  size_board = len(game.board)

  scoring = {'player1': 0, 'player2': 0}

  for lin in range(size_board):
    for col in range(size_board):

      if game.board[lin][col] not in [1,2]: continue

      # peça pertence ao jogador
      # correr vinzinhança à procura de vazio
      # se encontrar não está protegida

      player = game.board[lin][col]
      score = f'player{player}'

      # retorna 1 se correu a vizinhança toda e por isso está protected
      if neighbourhood_action(game=game, lin_origin=lin, col_origin=col, \
                       displacement_size=[-1,0,1], action=not_protected):

        scoring[score] += 2
      else:
        # se não está protegiada só vale 1
        scoring[score] += 1
        # setattr(scoring, score, \
        #         getattr(scoring, score) + 1)

  return scoring['player1'] - scoring['player2']

      
def compare(a: int, comparison: str, b: int) -> bool:
  if comparison == '>':
    return a > b

  if comparison == '<':
    return a < b


## Search functions

def plays_available(game: Type[Game], players: list) -> int:
  '''
  players = [1], [2] ou [1,2]
  determina se existe pelo menos um movimento possível de um jogador
  que esteja na lista players
  mas ambos têm de ter pelo menos uma peça
  retorna 0 :: se não existir
  retorna 1 :: se     existir
  '''
  
  # assumo a possibilidade de tabuleiros em que nem
  # todas as células vazias sejam atingíveis:
  # por exemplo fazendo uma wall grande around it
  # porque senão assumir isso, então todos as células "não parede" são
  # atingíveis e por isso basta haver uma célula vazia para implicar
  # a existência de movimento, a não ser que um do jogadores já não tenha peça

  if game.score_1 == 0 or game.score_2 == 0: return 0

  size_board = len(game.board)

  # vou procurar células vazias e depois procurar
  # na vizinhança peça de jogador na lista players fornecida
  
  for lin in range(size_board):
    for col in range(size_board):

      # só vale a pena procurar movimento se tiver uma peça de um jogador
      if game.board[lin][col] != 0: continue

      stay = neighbourhood_action(game=game, lin_origin=lin, col_origin=col, \
                           displacement_size=[-2,-1,0,1,2], action=cell_has_player_piece, \
                           players = players)

      if not stay: return 1

  return 0


def list_moves_player(game):
  '''calculate allowed moves'''

  size_board = len(game.board)

  moves = []
  clones_destinations = set()
  
  # need to iterate over board, find pieces and record where they can go.
  for lin in range(size_board):
    for col in range(size_board):

      if game.board[lin][col] != game.turn: continue
        
      for dlin in [-2,-1,0,1,2]:
        for dcol in [-2,-1,0,1,2]:

          adj_lin = lin + dlin
          adj_col = col + dcol

          # origem não interessa na pesquisa
          if dlin == 0 and dcol == 0: continue
          # se indice for negativo não são candidatos adjacentes
          if adj_lin < 0 or adj_col < 0: continue
          # fora do tabuleiro
          if adj_lin >= size_board or adj_col >= size_board: continue
          # lugar não vazio não interessa
          if game.board[adj_lin][adj_col] != 0: continue

          type_move = type_move_from_distance(lin, col, adj_lin, adj_col)
    
          if type_move == 1:
            # only 1 move for that destination is required
            # build a set to know if it's already there quickly i think
            if (adj_lin, adj_col) not in clones_destinations:
              clones_destinations.add( (adj_lin, adj_col) )
              moves.append( (lin, col, adj_lin, adj_col) )
            else: pass
            
          elif type_move == 2:
            moves.append( (lin, col, adj_lin, adj_col) )

  return moves


def list_plays_from_cell(game: Type[Game], lin: int, col: int) -> list:
  '''
  return a list of possible plays from a cell
  usado para pintar tabuleiro gráfico com possíveis destinos
  '''

  plays = []
  neighbourhood_action(game=game, lin_origin=lin, col_origin=col, \
                       displacement_size=[-2,-1,0,1,2], action=empty_cell, \
                       plays = plays)

  return plays


import time

def copy_game(game: Type[Game]) -> Type[Game]:

  size_board = len(game.board)

  trial = Game(size_board, True)
  trial.board = game.board.copy()
  trial.n_move = game.n_move
  trial.turn = game.turn
  trial.score_1 = game.score_1
  trial.score_2 = game.score_2

  return trial


def search_greedy_move(game: Type[Game]) -> Type[Move]:
  '''
  greedy search
  simula todas as possibilidade da pŕoxima jogada
  escolhe aquela com maior alteração ao score

  retorna 0 :: não tiver jogadas possíveis
  retorna move :: a melhor jogada escolhida
  '''
  size_board = len(game.board)

  # caso não se encontre jogada possível best_move mantém 0
  # e pc_player irá passar a vez

  best_move = 0
  best_score = 0
  
  plays = list_moves_player(game)
  
  for move in plays:
    start_lin, start_col, dest_lin, dest_col = move

    trial_game = copy_game(game)
    trial_move = Move(start_lin, start_col, dest_lin, dest_col)
    apply(trial_move, trial_game)

    # pode ser que haja mais que um move que dê o mesmo score
    # o primeiro a ser encontrado é o que fica
    score = f'score_{3-trial_game.turn}' # apply troca o turno

    if getattr(trial_game, score) > best_score:
      best_score = getattr(trial_game, score)
      best_move  = trial_move

  return best_move


from math import inf

def minimax(game, eval_fun, depth, alpha, beta, counter):
  '''
  minimax retorna:
  avaliação do ramo: int
  move: Type[Move]
  move de maior interesse para o jogador que tem a vez
  counter: int
  
  eval_fun :: avaliação
  depth    :: ...
  counter  :: contador de número de chamadas recursivas à função minimax
              top level call deve ser dado =0= como argumento

  se alpha && beta == None então não usa cortes
  '''

  best_move  =  0
  
  plays_available(game, [1,2])
  if depth == 0 or not plays_available(game, [1,2]):
    return eval_fun(game), best_move, counter

  plays = list_moves_player(game)
  constraints = {'alpha': alpha, \
                 'beta' : beta   }

  # player 1 -> Maximizing player
  if game.turn == 1:
    best_score = -inf
    comparison = '>'
    condition = 'alpha'
    
  # player 2 -> Minimizing player
  elif game.turn == 2:
    best_score = +inf
    comparison = '<'
    condition = 'beta'

  # DEBUG
  #name = 'all trials time: '
  #st = time.process_time_ns()
  for move in plays:
    start_lin, start_col, dest_lin, dest_col = move

    # follow to a children node
    # DEBUG
    #name = 'copy game time: '
    #st = time.process_time_ns()
    trial_game = copy_game(game)
    trial_move = Move(start_lin, start_col, dest_lin, dest_col)
    apply(trial_move, trial_game)
    #et = time.process_time_ns()
    #print(name, (et-st)//1000, 'µs')

    counter += 1
    # call to minimax must be with possibly updated alpha beta not the one the current function received as arguments
    evaluation, _, counter = minimax(trial_game, eval_fun, depth - 1, constraints['alpha'], constraints['beta'], counter)

    #          evaluation    > ou <    best_score
    if compare(evaluation, comparison, best_score):
      best_score = evaluation
      best_move  = trial_move

    # se alpha/beta foi passado como None, isto nunca executa
    if constraints[condition] != None:
      #          evaluation    > ou <    alpha ou beta  
      if compare(evaluation, comparison, constraints[condition]):
        constraints[condition] = evaluation

      if constraints['alpha'] >= constraints['beta']: break
      # irá retornar a maior eval que causou a invalidação das condições se no Maximizing player
      # irá retornar a menor eval que causou a invalidação das condições se no Minimizing player

  #et = time.process_time_ns()
  #print(name, (et-st)//1000, 'µs')

  if len(plays) == 0: # quer dizer que este jogador não tem jogadas e tem de passar a vez
    trial_game = copy_game(game)
    skip_turn(trial_game)
    counter += 1
    evaluation, _, counter = minimax(trial_game, eval_fun, depth - 1, constraints['alpha'], constraints['beta'], counter)

    if compare(evaluation, comparison, best_score):
      best_score = evaluation
      # não tenho como registar o skip_turn como move feito

    if constraints[condition] != None:
      #          evaluation    > ou <    alpha ou beta  
      if compare(evaluation, comparison, constraints[condition]):
        constraints[condition] = evaluation

      # não precisa disto porque não é um loop, não há mais nada a correr
      # if constraints['alpha'] >= constraints['beta']: break
      
  return best_score, best_move, counter

1. installing requirements
   python3 -m pip install -r requirements.txt --user

2. run graphic version
   python ataxx.py

  1. if window size is beyond monitor size, remove ", SCALED" from
  "screen = pg.display.set_mode( (real_screen_size, real_screen_size), SCALED)" in ataxx.py
  can also change "screen_size = 800" at the top of the same file
  

3. run command line version (BARE)
   python ataxx_cli.py

from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1' # disabling pygame hello message

import time
import pygame as pg
from pygame.locals import *

from core     import load_state, plays_available, skip_turn
from graphics import ( GGame, load_image, update_screen, select_cell_and_play,
                       pc_player, human_player, game_type_def, pc_player_type, board_size_def,
                       choose_load_or_new, graphical_board_load, graphical_board_save )

screen_size = 800 # tentative size, it might not be a multiple of cell_size which will be an int

### Main loop

def main():
  '''
  initialization and game loop
  '''
  
  load = choose_load_or_new()
  if not load:
    size_board = board_size_def()
    game = GGame(size_board, screen_size)
  else:
    game = graphical_board_load(screen_size)
  
  player1, player2, FPS = game_type_def()
  
  # setting type of pc_player
  if player1 == pc_player or player2 == pc_player:
    pc_player_type(game)

  print("\nESC: quit ::: F1: save game ::: F2: change theme ::: F3: soft reset ::: SPC: skip turn\n")
    
  pg.init()
  real_screen_size = game.cell_size * len(game.board)
  screen = pg.display.set_mode( (real_screen_size, real_screen_size), SCALED)
  pg.display.set_caption("Ataxx")
  
  # load assests
  # selected requires alpha transparency since it's overlaid on piece
  assets = { "empty"       : load_image("pixel_board_empty.png", game.cell_size),               \
             "blue"        : load_image("pixel_board_blue.png", game.cell_size),                \
             "red"         : load_image("pixel_board_red.png", game.cell_size),                 \
             "wall"        : load_image("pixel_board_wall.png", game.cell_size),                \
             "selected"    : load_image("pixel_board_selected.png", game.cell_size, -1),        \
             "destination" : load_image("pixel_board_possible_destination.png", game.cell_size) }

  assets2 = {"empty"       : load_image("platform_empty.png", game.cell_size),        \
             "blue"        : load_image("platform_cuphead.png", game.cell_size),      \
             "red"         : load_image("platform_ginger.png", game.cell_size),       \
             "wall"        : load_image("platform_wall.png", game.cell_size),         \
             "selected"    : load_image("platform_selected.png", game.cell_size, -1), \
             "destination" : load_image("platform_island.png", game.cell_size)        }

  clock = pg.time.Clock() 
  run = True
  
  while run :
    clock.tick(FPS)
    update_screen(screen, game, assets)

    # test before plays so that a next iteration occurs and we get the last play painted
    if not plays_available(game, [1,2]): run = False

    # plays in elif since otherwise in the end game these tests the pc_player would skip_turn
    # in case it becomes relevant to know on which turn the game ended
    
    elif game.turn == 1:
      # DEBUG
      #name = 'player 1 time: '
      #st = time.process_time_ns()
      human_turn = player1(game)
      #et = time.process_time_ns()
      #print(name, (et-st)//1000000, 'ms')
    elif game.turn == 2:
      #name = 'player 2 time: '
      #st = time.process_time_ns()
      human_turn = player2(game)
      #et = time.process_time_ns()
      #print(name, (et-st)//1000000, 'ms')

    for event in pg.event.get():
      if   event.type == QUIT:                              run = False
      elif event.type == KEYDOWN and event.key == K_ESCAPE: run = False

      elif event.type == MOUSEBUTTONDOWN and human_turn:
        select_cell_and_play(game, event)
        
      elif event.type == KEYDOWN and event.key == K_SPACE and human_turn:
        if not skip_turn(game):
          print("Can't skip, there are available plays")

      elif event.type == KEYDOWN and event.key == K_F1:
        graphical_board_save(game)
        
      elif event.type == KEYDOWN and event.key == K_F2:
        assets, assets2 = assets2, assets # change theme
        # force full board update
        # also commenting this proves board updates are only where changes happened
        game.cells2update = None
        
      elif event.type == KEYDOWN and event.key == K_F3:
        game = GGame(size_board, screen_size)

  
  # Game over 
  # winner declaration
  print("\nAnd the winner is: ", end = '')
  if game.score_1 > game.score_2:   print("Player 1!")
  elif game.score_1 < game.score_2: print("Player 2!")
  else:                               print("Just kidding, it's a tie.")

  print("Final piece count: ", end = '')
  print(game.score_1, game.score_2)

  # loop to wait to close the game
  run = True
  while run:
    clock.tick(20)
    
    for event in pg.event.get():
      if   event.type in (QUIT, KEYDOWN, MOUSEBUTTONDOWN): run = False

  pg.quit()


if __name__ == "__main__":
  main()


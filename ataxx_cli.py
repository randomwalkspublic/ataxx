## Functions for CLI
from os import system, name

def clear():
    # for windows
    if name == 'nt': system('cls')
    # for mac and linux(here, os.name is 'posix')
    else: system('clear')

    
from core import *

def human_player(game):
  '''
  cli_implementation
  '''

  while True:
    print("Insira move pretendido (exemplo: 0 0 1 1)")
    # não vou fazer checks
    lin_i, col_i, lin_f, col_f = map( int, input().split())
    changes = apply( Move(lin_i, col_i, lin_f, col_f) \
                      , game)

    success = len(changes)

    if success == 1: return 1
    else:            return 0

def pc_player(game):

  move = search_greedy_move(game)

  if move == 0:
    skip_turn(game)
    return 0

  return apply(move, game)
    
def game_type_def():
  '''
  Decidir:
  Humano vs Humano
  Humano vs PC
  PC     vs Humano
  PC     vs PC
  '''

  while True:
    clear()
    message = '''\
1 :: Humano vs Humano
2 :: Humano vs PC
3 :: PC vs Humano
4 :: PC vs PC'''
    print(message)
    print("Choose game type (default = 2): ", end = '')
    choice = input()

    if choice == '': choice = '2'

    if choice == '1':
      return (human_player, human_player)
    if choice == '2':
      return (human_player, pc_player)
    if choice == '3':
      return (pc_player, human_player)
    if choice == '4':
      return (pc_player,pc_player)

## CLI Game

# criar o jogo

game = Game()

# set up players
player1, player2 = game_type_def()

while plays_available(game, [1,2]):
  clear()
  print(game)
  if game.turn == 1:
    player1(game)
  else:
    player2(game)

  print(game)
  input() # press enter to continue to next loop

# winner enunciation
if game.score_1 > game.score_2: end_game = "player1"
elif game.score_1 < game.score_2: end_game = "player2"
else: end_game = "tie"

print(end_game)
